﻿using System;

namespace Greeting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please introduce yourself time traveler!");
            string name = Console.ReadLine();

            Console.WriteLine("Greetings " + name + "! Today:" + DateTime.Today.ToShortDateString());
        }
    }
}
