﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            var companies = new List<CompanyData>
            {
                new CompanyData ("Alfreid Futterkiste", "Maria Anders", "Germany"),
                new CompanyData ("Centro comecial Moctezuma", "Francisco Chang", "Mexico"),
                new CompanyData("Ernest Hande", "Ronald Mendel", "Austria"),
                new CompanyData("Island Trading", "Helen Bennett", "UK"),
                new CompanyData("Island Trading", "Helen Bennett", "UK"),
                new CompanyData("Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada"),
                new CompanyData("Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy"),
                new CompanyData("Island Trading", "Maria Mendel", "USA")
            };

            var contacts_opatators = (from p in companies
                            where p.Company == "Island Trading"
                            where p.Country.StartsWith('U')
                            select p.Contact).Distinct().OrderByDescending(p => p);

            var contacts_methods = companies
                .Where(p => p.Company == "Island Trading")
                .Where(p => p.Country.StartsWith('U'))
                .Select(p => p.Contact)
                .Distinct()
                .OrderByDescending(p => p);

            foreach (var item in contacts_opatators)
            {
                Console.WriteLine(item);
            }

            foreach (var item in contacts_methods)
            {
                Console.WriteLine(item);
            }
        }
    }
}