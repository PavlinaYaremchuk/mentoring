﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task6
{
    public class CompanyData
    {
        public CompanyData(string company, string contact, string country)
        {
            Company = company;
            Contact = contact;
            Country = country;
        }

        public string Company { get; set; }
        public string Contact { get; set; }
        public string Country { get; set; }
    }
}
