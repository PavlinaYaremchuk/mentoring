﻿namespace Task5
{
    class RecursiveSum
    {
        static int RecursiveSumOfDigits(int input)
        {
            int sum = 0;
            while (input > 0)
            {
                sum += input % 10;
                input /= 10;
            }
            if (sum >= 10)
            {
                return RecursiveSumOfDigits(sum);

            }
            return sum;
        }
    }
}
