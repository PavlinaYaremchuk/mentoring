﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Task5
{
    class ListOfGuests
    {
        static string SortedListOfGuests(string inputList)
        {
            List<KeyValuePair<string, string>> guestList = new List<KeyValuePair<string, string>>();
            foreach (string element in inputList.ToUpper().Split(';'))
            {
                var newList = element.Split(':');
                var pair = new KeyValuePair<string, string>(newList[1], newList[0]);
                guestList.Add(pair);
            }
            var result = guestList.OrderBy(guest => guest.Key).ThenBy(guest => guest.Value);
            return "(" + string.Join(")(", result.Select(guest => guest.Key + "," + guest.Value)) + ")";

        }
    }
}
