﻿using System;
using System.Collections.Generic;

namespace Task5
{
    public class ListFilterer
    {
        static public  List<int> GetIntegersFromList(List<object> listOfIntegersAndStrings)
        {
            List<int> integersFromList = new List<int>();
            foreach (var item in listOfIntegersAndStrings)
            {
                if (item is int)
                {
                    integersFromList.Add((int)item);
                }
            }
            return integersFromList;
        }
    }
}
