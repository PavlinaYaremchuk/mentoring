﻿using System;
using System.Linq;

namespace Task5
{
    public class FirstNonRepeatingLetter
    {
        static public string First_non_repeating_letter(string input)
        {
            //string result = "";
            //for (int i = 0; i < input.Length - 1; i++)

            //{
            //    var newArray = input.Remove(i, 1);
            //    if (!newArray.Contains(input[i], StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        result = input[i].ToString();
            //        break;
            //    }
            //}
            //return result;

            for (int i = 0; i < input.Length - 1; i++)
            {
                if (input.ToLowerInvariant().Where(ch => ch == char.ToLowerInvariant(input[i])).Count() == 1) 
                    return input[i].ToString();
            }
            return "";
        }
    }
}
