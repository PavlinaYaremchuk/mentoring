﻿using System;

namespace Guess_a_number
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int generatedValue = rnd.Next(0, 9);
            int attemptNumber = 0;
            int userInput;
            do
            {
                Console.WriteLine("Please, guess the number from 0 to 9, entering it");
                userInput = Convert.ToInt32(Console.ReadLine());
                attemptNumber++;

                if (userInput > generatedValue)
                {
                    Console.WriteLine("The entered number is greater than the specified one");
                }
                else if (userInput < generatedValue)
                {
                    Console.WriteLine("The entered number is smaller than the specified one");
                }
                else
                {
                    Console.WriteLine($"Congratulation! You guessed the number. It took you just {attemptNumber} attempts");
                }
            }
            while (userInput != generatedValue);
        }
    }
}