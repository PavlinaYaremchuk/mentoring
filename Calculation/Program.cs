﻿using System;

namespace Calculation
{
    class Program
    {
        public static int Sum(int number1, int number2)
        {
            return number1 + number2;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter two numbers separated by a space and press Enter");
            string[] temp = Console.ReadLine().Split(' ');

            int number1 = Convert.ToInt32(temp[0]);
            int number2 = Convert.ToInt32(temp[1]);

            Console.WriteLine($"Sum: {Sum(number1, number2)}");
        }
    }
}
